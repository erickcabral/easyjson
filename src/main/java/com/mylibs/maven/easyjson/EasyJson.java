/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mylibs.maven.easyjson;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 *
 * @author Erick Cabral
 */
public class EasyJson {

	private final Gson gsonUtil = new Gson();

	public String gsonConvertion2Json(Object object) {
		String result = null;
		try {
			result = gsonUtil.toJson(object);
		}
		catch (Exception e) {
			System.out.println("ERROR WHILE CONVERTING TO JSON: " + e.getMessage());
		}
		return result;
	}

	public <T> T gsonConvert2Java(String javaString, Class<T> sourceClass) {
		T result = null;
		try {
			result = gsonUtil.fromJson(javaString, sourceClass);
		}
		catch (JsonSyntaxException e) {
			System.out.println("ERROR WHILE CONVERTING FROM JSON: " + e.getMessage());
		}
		return result;
	}
}
