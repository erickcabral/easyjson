/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mylibs.maven.easyjson.EasyJson;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Erick Cabral
 */
public class EasyJsonTest {

	public EasyJsonTest() {
	}

	EasyJson easyJson;
	Person person;
	String jsonString = "";

	@Before
	public void setUp() {
		this.easyJson = new EasyJson();

		this.person = new Person("jhon", "Doe", "jhonDoe@test.com");
	}

	@Test
	public void gSONconverting2Json() {
		this.jsonString = this.easyJson.gsonConvertion2Json(this.person);
		System.out.println("Json String ->" + this.jsonString);
	}

	@Test
	public void gSONconverting2Java() {
		this.jsonString = this.easyJson.gsonConvertion2Json(this.person);
		Person personTest = this.easyJson.gsonConvert2Java(this.jsonString, Person.class);
		System.out.println("PERSON -> " + personTest.getName());
		System.out.println("PERSON -> " + personTest.getSurname());
		System.out.println("PERSON -> " + personTest.getEmail());
	}
}

class Person {

	public Person(String name, String surname, String email) {
		this.name = name;
		this.surname = surname;
		this.email = email;
	}

	private String name, surname, email;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
